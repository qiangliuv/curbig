# cu-Rotation-Based Iterative Gaussianization

**GOAL**: Accelerate Computing of RBIG 

**Useful Links:**

- Lab Webpage: [isp.uv.es](isp.uv.es)
- MATLAB Code: [webpage](https://github.com/IPL-UV/rbig_matlab)
- Original Python Code - [pyRBIG](https://github.com/spencerkent/pyRBIG)
- Iterative Gaussianization: [from ICA to Random Rotations](https://arxiv.org/abs/1602.00229)
- Implementation RBIG that compatible with the scikit-learn framework version: [rbig](https://github.com/IPL-UV/rbig)

The Problem of Above Implementation: 

Before we start to talk about pitfall of RBIG, let's first introduced basic mechanism of RBIG.

**RBIG**, an approach is used to transform any multi-dimensional distribution to a Gaussian distribution.                                                                                                                                              

The RBIG basic working steps: 
	
	initial non-Gaussian distribution data -> marginal gaussianzation for each dimentional of initial data -> Rotation with random(worse than pca)/PCA(slow)/ICA(too slow) ... -> Gaussiza distribution sapce.

Now, the main problem of BRIG is speed problem specific when we feeding big dimentional datset into it [e.g., our Entroy2020 paper].
The one way to solve above problem is GPU. In the past few years GPUs have far surpassed the computational capabilities of CPUs for 
floating arithmetic. In this implementation, we implemented the [**CUDAMat**](https://github.com/cudamat/cudamat) library to 
accelerate computing of RBIG. We named this version implementation is: **cuRBIG**.

Secondly, to speed up RBIG, we should consider making RBIG **Hierarchical** running, which faces a few technical challenges.
But it will speed up BRIG more.

***one example of CUDAMat***,


```python
#create matrix on GPU
A = cm.CUDAMatrix ( np.random .randn (5 , 10))
T = cm.CUDAMatrix ( np.random .randn (5 , 10))
#exp operation
cm .exp (A ,target = T)
#log operation
B = cm .log (A)
# convert gpu gdarray to cpu ndarray
cpu_array = B.asarray()
```

The more example and help documents link [here](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.232.4776&rep=rep1&type=pdf).