#!usr/bin/env python3
# -*- coding: utf-8 -*-

'''
======================================================================
Rotation-Based Iterative Gaussian-ization (RBIG) GPU Version 3.0
Copyright(c) 2020  Qiang Li
All Rights Reserved.
qiang.li@uv.es
Distributed under the (new) BSD License.
######################################################################
'''

import numpy as np
from sklearn.utils import check_random_state, check_array
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import QuantileTransformer
from sklearn.decomposition import PCA, FastICA
from sklearn.model_selection import train_test_split
from sklearn.metrics import normalized_mutual_info_score as mi_score
from scipy.stats import norm, uniform, ortho_group, entropy as sci_entropy
from scipy import stats
from scipy.interpolate import interp1d
import warnings
import sys
import logging
from tqdm import trange
import heartrate
heartrate.trace(browser=True)

logging.basicConfig(
    level=logging.INFO,
    stream=sys.stdout,
    format="%(asctime)s: %(levelname)s: %(message)s",
)
logger = logging.getLogger()
logger.setLevel(logging.INFO)

warnings.filterwarnings("ignore")

logging.debug(f"Transforming Feature")

import gc 

try:
    import cudamat as cm
    GPU_SUPPORT = True
except:
    print("Install CUDA and cudamat (for python) to enable GPU speedups.")
    GPU_SUPPORT = False


class cuRBIG(object):
    """ Rotation-Based Iterative Gaussian-ization (RBIG). This algorithm transforms
    any multidimensional data to a Gaussian. It also provides a sampling mechanism
    whereby you can provide multidimensional gaussian data and it will generate 
    multidimensional data in the original domain. You can calculate the probabilities
    as well as have access to a few information theoretic measures like total 
    correlation and entropy.

    Parameters
    ----------
    n_layers : int, optional (default 1000)
        The number of steps to run the sequence of marginal gaussianization
        and then rotation

    rotation_type : {'PCA', 'random'}
        The rotation applied to the marginally Gaussian-ized data at each iteration.
        - 'pca'     : a principal components analysis rotation (PCA)
        - 'random'  : random rotations
        - 'ica'     : independent components analysis (ICA)

    pdf_resolution : int, optional (default 1000)
        The number of points at which to compute the gaussianized marginal pdfs.
        The functions that map from original data to gaussianized data at each
        iteration have to be stored so that we can invert them later - if working
        with high-dimensional data consider reducing this resolution to shorten
        computation time.

    pdf_extension : int, optional (default 0.1)
        The fraction by which to extend the support of the Gaussian-ized marginal
        pdf compared to the empirical marginal PDF.

    verbose : int, optional
        If specified, report the RBIG iteration number every
        progress_report_interval iterations.

    zero_tolerance : int, optional (default=60)
        The number of layers where the total correlation should not change
        between RBIG iterations. If there is no zero_tolerance, then the
        method will stop iterating regardless of how many the user sets as
        the n_layers.

    rotation_kwargs : dict, optional (default=None)
        Any extra keyword arguments that you want to pass into the rotation
        algorithms (i.e. ICA or PCA). See the respective algorithms on 
        scikit-learn for more details.

    random_state : int, optional (default=None)
        Control the seed for any randomization that occurs in this algorithm.

    entropy_correction : bool, optional (default=True)
        Implements the shannon-millow correction to the entropy algorithm

    Attributes
    ----------
    gauss_data : array, (n_samples x d_dimensions)
        The gaussianized data after the RBIG transformation

    residual_info : array, (n_layers)
        The cumulative amount of information between layers. It should exhibit
        a curve with a plateau to indicate convergence.

    rotation_matrix = dict, (n_layers)
        A rotation matrix that was calculated and saved for each layer.

    gauss_params = dict, (n_layers)
        The cdf and pdf for the gaussianization parameters used for each layer.

    References
    ----------
    * Original Paper : Iterative Gaussianization: from ICA to Random Rotations
        https://arxiv.org/abs/1602.00229

    * Original MATLAB Implementation
        http://isp.uv.es/rbig.html

    * Original Python Implementation [Version 1.0]
        https://github.com/spencerkent/pyRBIG
    
    * Updated Python Implementation  [Version 2.0]
        https://github.com/jejjohnson/rbig
    """


    def __init__(self, n_layers=1000, rotation_type="PCA", random_state=None, tolerance=None, zero_tolerance=60, 
        pdf_resolution=1000, pdf_extension=None, entropy_correction=True, rotation_kwargs=None, base="gauss", seed=None, verbose=False, gpu=False,
        missing_values=None, gaussianize='standard',):
        
        self.n_layers = n_layers
        self.rotation_type = rotation_type
        self.pdf_resolution = pdf_resolution
        self.pdf_extension = pdf_extension
        self.random_state = random_state
        self.tolerance = tolerance
        self.zero_tolerance = zero_tolerance
        self.entropy_correction = entropy_correction
        self.rotation_kwargs = rotation_kwargs
        self.base = base
        self.missing_values = missing_values
        self.gaussianize = gaussianize 
        self.gpu = gpu
        
        np.random.seed(seed)  # Set for deterministic results
        self.verbose = verbose
        if verbose > 0:
            np.set_printoptions(precision=3, suppress=True, linewidth=200)
            print('RBIG, Layers: {}'.format(n_layers))
        if verbose > 1:
            np.seterr(all='warn')
        else:
            np.seterr(all='ignore')

        self.gpu = gpu  # Enable GPU support for some large matrix multiplications.
        if self.gpu:
            cm.cublas_init()

        # Initialize these when we fit on data
         n_samples, n_dimensions = 0, 0  # Number of samples/variables in input data
        # Initialize stopping criteria (residual information)
        self.residual_info = list()
        self.gauss_params = list()
        self.rotation_matrix = list()

    def fit(self, X):
        """ Fit the model with X.
        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
            Training data, where n_samples in the number of samples
            and n_features is the number of features.

        Returns
        -------
        self : object
            Returns the instance itself.
        """
        X = check_array(X, ensure_2d=True)
        self._fit(X)
        return self

    def _fit(self, data):
        """ Fit the model with data.
        Parameters
        ----------
        data : array-like, shape (n_samples, n_features)
            Training data, where n_samples in the number of samples
            and n_features is the number of features.
        Returns
        -------
        self : object
            Returns the instance itself.
        """
        data = check_array(data, ensure_2d=True)
        #x = np.asarray(x, dtype=np.float32)
        self.X_fit_ = data
        gauss_data = np.copy(data)

        n_samples, n_dimensions = np.shape(data)

        ################################################################
        # Maybe I don't need this section
        ################################################################
        if self.zero_tolerance is None:
            self.zero_tolerance = self.n_layers + 1

        if self.tolerance is None:
            self.tolerance = self._get_information_tolerance(n_samples)
        #################################################################
        logging.debug("Data (shape): {}".format(np.shape(gauss_data)))

        # Loop through the layers
        logging.debug("Running: Looping through the layers...")
        for layer in trange(self.n_layers, desc = 'Gaussian and Rotation .......'):
            if self.verbose > 1:
                print("Completed {} iterations of RBIG.".format(layer + 1))

            # ------------------
            # Gaussian(-ization)
            # ------------------
            layer_params = list()

            for idim in range(n_dimensions):

                gauss_data[:, idim], temp_params = self.univariate_make_normal(
                    gauss_data[:, idim], self.pdf_extension, self.pdf_resolution
                )

                # append the parameters
                layer_params.append(temp_params)

            self.gauss_params.append(layer_params)
            gauss_data_prerotation = gauss_data.copy()
            if self.verbose == 2:
                print(gauss_data.min(), gauss_data.max())

            # --------
            # Rotation
            # --------
            if self.rotation_type == "random":

                rand_ortho_matrix = ortho_group.rvs(n_dimensions)
                
                if self.gpu:
                    targe_a = cm.empty( n_samples, n_dimensions) 
                    gauss_data_cm = cm.CUDAMatrix(gauss_data)
                    rand_ortho_matrix_cm = cm.CUDAMatrix(rand_ortho_matrix)
                    cm.dot(gauss_data_cm, rand_ortho_matrix, target = targe_a)
                    gauss_data = targe_a.asarray()
                    del gauss_data_cm
                    del rand_ortho_matrix_cm
                    del targe_a
                    self.rotation_matrix.append(rand_ortho_matrix)
                else:
                    gauss_data = np.dot(gauss_data, rand_ortho_matrix)
                    self.rotation_matrix.append(rand_ortho_matrix)

            elif self.rotation_type.lower() == "ica":

                # initialize model fastica model
                if self.rotation_kwargs is not None:
                    ica_model = FastICA(
                        random_state=self.random_state, **self.rotation_kwargs
                    )
                else:
                    ica_model = FastICA(random_state=self.random_state)
                # fit-transform data
                gauss_data = ica_model.fit_transform(gauss_data)

                # save rotation matrix
                self.rotation_matrix.append(ica_model.components_.T)

            elif self.rotation_type.lower() == "pca":

                # Initialize PCA model
                if self.rotation_kwargs is not None:
                    pca_model = PCA(
                        random_state=self.random_state, **self.rotation_kwargs
                    )
                else:
                    pca_model = PCA(random_state=self.random_state)

                logging.debug("Size of gauss_data: {}".format(gauss_data.shape))
                gauss_data = pca_model.fit_transform(gauss_data)
                self.rotation_matrix.append(pca_model.components_.T)

            else:
                raise ValueError(
                    "Rotation type " + self.rotation_type + " not recognized"
                )
            
            # --------------------------------
            # Information Reduction
            # --------------------------------
            self.residual_info.append(
                information_reduction(
                    gauss_data, gauss_data_prerotation, self.tolerance
                )
            )

            # --------------------------------
            # Stopping Criteria
            # --------------------------------
            if self._stopping_criteria(layer):
                break
            else:
                pass
            self.residual_info = np.array(self.residual_info)
            self.gauss_data = gauss_data
            self.mutual_information = np.sum(self.residual_info)
            self.n_layers = len(self.gauss_params)

        return self
    
    def _stopping_criteria(self, layer):
        """Stopping criteria for the the RBIG algorithm.
        
        Parameter
        ---------
        layer : int

        Returns
        -------
        verdict = 
        
        """
        stop_ = False

        if layer > self.zero_tolerance:
            aux_residual = np.array(self.residual_info)

            if np.abs(aux_residual[-self.zero_tolerance :]).sum() == 0:
                logging.debug("Done! aux: {}".format(aux_residual))

                # delete the last 50 layers for saved parameters
                self.rotation_matrix = self.rotation_matrix[:-50]
                self.gauss_params = self.gauss_params[:-50]

                stop_ = True
            else:
                stop_ = False

        return stop_
    
    def transform(self, x):
        """Complete transformation of X given the learned Gaussianization parameters.
        This assumes that the data follows a similar distribution as the data that
        was original used to fit the RBIG Gaussian-ization parameters.
        
        Parameters 
        ----------
        x : array, (n_samples, n_dimensions)
            The data to be transformed (Gaussianized)
            
        Returns
        -------
        x_transformed : array, (n_samples, n_dimensions)
            The new transformed data in the Gaussian domain

        """
        x_transformed = np.copy(x)

        for layer in trange(self.n_layers, desc = 'Gaussian and Rotation ......'):

            # ----------------------------
            # Marginal Uniformization
            # ----------------------------
            data_layer = x_transformed

            for idim in range(n_dimensions):

                # marginal uniformization
                data_layer[:, idim] = interp1d(
                    self.gauss_params[layer][idim]["uniform_cdf_support"],
                    self.gauss_params[layer][idim]["uniform_cdf"],
                    fill_value="extrapolate",
                )(data_layer[:, idim])

                # marginal gaussianization
                data_layer[:, idim] = norm.ppf(data_layer[:, idim])

            # ----------------------
            # Rotation
            # ----------------------
            if self.gpu:

                targe_b = cm.empty( n_samples, n_dimensions)
                data_layer_cm = cm.CUDAMatrix(data_layer)
                cm.dot(data_layer_cm, self.rotation_matrix[layer], target = targe_b)
                x_transformed = targe_b.asarray()
                del targe_b
                del data_layer_cm
            else:
                x_transformed = np.dot(data_layer, self.rotation_matrix[layer])

        return x_transformed

    def inverse_transform(self, x):
        """Complete transformation of X in the  given the learned Gaussianization parameters.

        Parameters
        ----------
        x : array, (n_samples, n_dimensions)
            The X that follows a Gaussian distribution to be transformed
            to data in the original input space.

        Returns
        -------
        X_input_domain : array, (n_samples, n_dimensions)
            The new transformed X in the original input space.

        """
        x_input_domain = np.copy(x)

        for layer in trange(self.n_layers - 1, -1, -1):

            if self.verbose > 1:
                print("Completed {} inverse iterations of RBIG.".format(layer + 1))

            if self.gpu:

                targe_c = cm.empty( n_samples, n_dimensions)
                x_input_domain_cm = cm.CUDAMatrix(x_input_domain)
                cm.dot(x_input_domain_cm, self.rotation_matrix[layer].T, target = targe_c)
                x_input_domain = targe_c.asarray()
                del targe_c
                del x_input_domain_cm

            else:

                x_input_domain = np.dot(x_input_domain, self.rotation_matrix[layer].T)

            temp = x_input_domain
            for idim in range(n_dimensions):
                temp[:, idim] = self.univariate_invert_normalization(
                    temp[:, idim], self.gauss_params[layer][idim]
                )
            x_input_domain = temp

        return x_input_domain

    def _get_information_tolerance(self, n_samples):
        """Precompute some tolerances for the tails."""
        xxx = np.logspace(2, 8, 7)
        yyy = [0.1571, 0.0468, 0.0145, 0.0046, 0.0014, 0.0001, 0.00001]

        return interp1d(xxx, yyy)(n_samples)


    def jacobian(self, x, return_x_transform=False):
        """Calculates the jacobian matrix of the x.

        Parameters
        ----------
        x : array, (n_samples, n_features)
            The input array to calculate the jacobian using the Gaussianization params.

        return_x_transform : bool, default: False
            Determines whether to return the transformed Data. This is computed along
            with the Jacobian to save time with the iterations

        Returns
        -------
        jacobian : array, (n_samples, n_features, n_features)
            The jacobian of the data w.r.t. each component for each direction

        x_transformed : array, (n_samples, n_features) (optional)
            The transformed data in the Gaussianized space
        """
        n_samples, n_components = x.shape

        # initialize jacobian matrix
        jacobian = np.zeros((n_samples, n_components, n_components))

        x_transformed = x.copy()

        XX = np.zeros(shape=(n_samples, n_components))
        XX[:, 0] = np.ones(shape=n_samples)

        # initialize gaussian pdf
        gaussian_pdf = np.zeros(shape=(n_samples, n_components, self.n_layers))
        igaussian_pdf = np.zeros(shape=(n_samples, n_components))

        # TODO: I feel like this is repeating a part of the transform operation

        for ilayer in range(self.n_layers):

            for idim in range(n_components):

                # Marginal Uniformization
                data_uniform = interp1d(
                    self.gauss_params[ilayer][idim]["uniform_cdf_support"],
                    self.gauss_params[ilayer][idim]["uniform_cdf"],
                    fill_value="extrapolate",
                )(x_transformed[:, idim])

                # Marginal Gaussianization
                igaussian_pdf[:, idim] = norm.ppf(data_uniform)

                # Gaussian PDF
                gaussian_pdf[:, idim, ilayer] = interp1d(
                    self.gauss_params[ilayer][idim]["empirical_pdf_support"],
                    self.gauss_params[ilayer][idim]["empirical_pdf"],
                    fill_value="extrapolate",
                )(x_transformed[:, idim]) * (1 / norm.pdf(igaussian_pdf[:, idim]))

            XX = np.dot(gaussian_pdf[:, :, ilayer] * XX, self.rotation_matrix[ilayer])

            x_transformed = np.dot(igaussian_pdf, self.rotation_matrix[ilayer])
        jacobian[:, :, 0] = XX

        if n_components > 1:

            for idim in range(n_components):

                XX = np.zeros(shape=(n_samples, n_components))
                XX[:, idim] = np.ones(n_samples)

                for ilayer in range(self.n_layers):

                    XX = np.dot(
                        gaussian_pdf[:, :, ilayer] * XX, self.rotation_matrix[ilayer]
                    )

                jacobian[:, :, idim] = XX

        if return_x_transform:
            return jacobian, x_transformed
        else:
            return jacobian

    def predict_proba(self, x, n_trials=1, chunksize=2000, domain="input"):
        """ Computes the probability of the original data under the generative RBIG
        model.

        Parameters
        ----------
        x : array, (n_samples x n_components)
            The points that the pdf is evaluated

        n_trials : int, (default : 1)
            The number of times that the jacobian is evaluated and averaged

        TODO: make sure n_trials is an int
        TODO: make sure n_trials is 1 or more

        chunksize : int, (default: 2000)
            The batchsize to calculate the jacobian matrix.

        TODO: make sure chunksize is an int
        TODO: make sure chunk size is greater than 0

        domain : {'input', 'gauss', 'both'}
            The domain to calculate the PDF.
            - 'input' : returns the original domain (default)
            - 'gauss' : returns the gaussian domain
            - 'both'  : returns both the input and gauss domain

        Returns
        -------
        prob_data_input_domain : array, (n_samples)
            The probability
        """
        component_wise_std = np.std(x, axis=0) / 20

        n_samples, n_components = x.shape

        prob_data_gaussian_domain = np.zeros(shape=(n_samples, n_trials))
        prob_data_input_domain = np.zeros(shape=(n_samples, n_trials))

        for itrial in range(n_trials):

            jacobians = np.zeros(shape=(n_samples, n_components, n_components))

            if itrial < n_trials:
                data_aux = x + component_wise_std[None, :]
            else:
                data_aux = x

            data_temp = np.zeros(data_aux.shape)

            jacobians, data_temp = self.jacobian(data_aux, return_X_transform=True)
            # set all nans to zero
            jacobians[np.isnan(jacobians)] = 0.0

            # get the determinant of all jacobians
            det_jacobians = np.linalg.det(jacobians)

            # Probability in Gaussian Domain
            prob_data_gaussian_domain[:, itrial] = np.prod(
                (1 / np.sqrt(2 * np.pi)) * np.exp(-0.5 * np.power(data_temp, 2)), axis=1
            )

            # set all nans to zero
            prob_data_gaussian_domain[np.isnan(prob_data_gaussian_domain)] = 0.0

            # compute determinant for each sample's jacobian
            prob_data_input_domain[:, itrial] = prob_data_gaussian_domain[
                :, itrial
            ] * np.abs(det_jacobians)

            # set all nans to zero
            prob_data_input_domain[np.isnan(prob_data_input_domain)] = 0.0

        # Average all the jacobians we calculate
        prob_data_input_domain = prob_data_input_domain.mean(axis=1)
        prob_data_gaussian_domain = prob_data_gaussian_domain.mean(axis=1)
        det_jacobians = det_jacobians.mean()

        # save the jacobians
        self.jacobians = jacobians
        self.det_jacobians = det_jacobians

        if domain == "input":
            return prob_data_input_domain
        elif domain == "transform":
            return prob_data_gaussian_domain
        elif domain == "both":
            return prob_data_input_domain, prob_data_gaussian_domain

    def entropy(self, correction=None):

        # TODO check fit
        if (correction is None) or (correction is False):
            correction = self.entropy_correction
        return (
            entropy_marginal(x, correction=correction).sum()
            - self.mutual_information
        )

    def total_correlation(self):

        # check fit
        return self.residual_info.sum()

    def univariate_make_normal(self, uni_data, extension, precision):
        """
        Takes univariate data and transforms it to have approximately normal dist
        We do this through the simple composition of a histogram equalization
        producing an approximately uniform distribution and then the inverse of the
        normal CDF. This will produce approximately gaussian samples.
        Parameters
        ----------
        uni_data : ndarray
        The univariate data [Sx1] where S is the number of samples in the dataset
        extension : float
        Extend the marginal PDF support by this amount.
        precision : int
        The number of points in the marginal PDF

        Returns
        -------
        uni_gaussian_data : ndarray
        univariate gaussian data
        params : dictionary
        parameters of the transform. We save these so we can invert them later
        """
        data_uniform, params = self.univariate_make_uniform(
            uni_data.T, extension, precision
        )
        if self.base == "gauss":
            return norm.ppf(data_uniform).T, params
        elif self.base == "uniform":
            return uniform.ppf(data_uniform).T, params
        else:
            raise ValueError(f"Unrecognized base dist: {self.base}.")

    def univariate_make_uniform(self, uni_data, extension, precision):
        """
        Takes univariate data and transforms it to have approximately uniform dist
        Parameters
        ----------
        uni_data : ndarray
        The univariate data [1xS] where S is the number of samples in the dataset
        extension : float
        Extend the marginal PDF support by this amount. Default 0.1
        precision : int
        The number of points in the marginal PDF
        Returns
        -------
        uni_uniform_data : ndarray
        univariate uniform data
        transform_params : dictionary
        parameters of the transform. We save these so we can invert them later
        """
        n_samps = len(uni_data)
        support_extension = (extension / 100) * abs(np.max(uni_data) - np.min(uni_data))

        # not sure exactly what we're doing here, but at a high level we're
        # constructing bins for the histogram
        bin_edges = np.linspace(
            np.min(uni_data), np.max(uni_data), int(np.sqrt(np.float64(n_samps)) + 1)
        )
        bin_centers = np.mean(np.vstack((bin_edges[0:-1], bin_edges[1:])), axis=0)

        counts, _ = np.histogram(uni_data, bin_edges)

        bin_size = bin_edges[2] - bin_edges[1]
        pdf_support = np.hstack(
            (bin_centers[0] - bin_size, bin_centers, bin_centers[-1] + bin_size)
        )
        empirical_pdf = np.hstack((0.0, counts / (np.sum(counts) * bin_size), 0.0))
        # ^ this is unnormalized
        c_sum = np.cumsum(counts)
        cdf = (1 - 1 / n_samps) * c_sum / n_samps

        incr_bin = bin_size / 2

        new_bin_edges = np.hstack(
            (
                np.min(uni_data) - support_extension,
                np.min(uni_data),
                bin_centers + incr_bin,
                np.max(uni_data) + support_extension + incr_bin,
            )
        )

        extended_cdf = np.hstack((0.0, 1.0 / n_samps, cdf, 1.0))
        new_support = np.linspace(new_bin_edges[0], new_bin_edges[-1], int(precision))
        learned_cdf = interp1d(new_bin_edges, extended_cdf)
        uniform_cdf = make_cdf_monotonic(learned_cdf(new_support))
        # ^ linear interpolation
        uniform_cdf /= np.max(uniform_cdf)
        uni_uniform_data = interp1d(new_support, uniform_cdf)(uni_data)

        return (
            uni_uniform_data,
            {
                "empirical_pdf_support": pdf_support,
                "empirical_pdf": empirical_pdf,
                "uniform_cdf_support": new_support,
                "uniform_cdf": uniform_cdf,
            },
        )

    def univariate_invert_normalization(self, uni_gaussian_data, trans_params):
        """
        Inverts the marginal normalization
        See the companion, univariate_make_normal.py, for more details
        """
        if self.base == "gauss":
            uni_uniform_data = norm.cdf(uni_gaussian_data)
        elif self.base == "uniform":
            uni_uniform_data = uniform.cdf(uni_gaussian_data)
        else:
            raise ValueError(f"Unrecognized base dist.: {base}.")

        uni_data = self.univariate_invert_uniformization(uni_uniform_data, trans_params)
        return uni_data

    def univariate_invert_uniformization(self, uni_uniform_data, trans_params):
        """
        Inverts the marginal uniformization transform specified by trans_params
        See the companion, univariate_make_normal.py, for more details
        """
        # simple, we just interpolate based on the saved CDF
        return interp1d(
            trans_params["uniform_cdf"], trans_params["uniform_cdf_support"]
        )(uni_uniform_data)

    def preprocess(self, x, fit=False):
        """Transform each marginal to be as close to a standard Gaussian as possible.
        'standard' (default) just subtracts the mean and scales by the std.
        'empirical' does an empirical gaussianization (but this cannot be inverted).
        'outliers' tries to squeeze in the outliers
        Any other choice will skip the transformation."""
        if self.missing_values is not None:
            x, self.n_obs = mean_impute(x, self.missing_values)  # Creates a copy
        else:
            self.n_obs = len(x)
        if self.gaussianize == 'none':
            pass
        elif self.gaussianize == 'standard':
            if fit:
                mean = np.mean(x, axis=0)
                # std = np.std(x, axis=0, ddof=0).clip(1e-10)
                std = np.sqrt(np.sum((x - mean)**2, axis=0) / self.n_obs).clip(1e-10)
                self.theta = (mean, std)
            x = ((x - self.theta[0]) / self.theta[1])
            if np.max(np.abs(x)) > 6 and self.verbose:
                print("Warning: outliers more than 6 stds away from mean. Consider using gaussianize='outliers'")
        elif self.gaussianize == 'outliers':
            if fit:
                mean = np.mean(x, axis=0)
                std = np.std(x, axis=0, ddof=0).clip(1e-10)
                self.theta = (mean, std)
            x = g((x - self.theta[0]) / self.theta[1])  # g truncates long tails
        elif self.gaussianize == 'empirical':
            print("Warning: correct inversion/transform of empirical gauss transform not implemented.")
            x = np.array([norm.ppf((rankdata(x_i) - 0.5) / len(x_i)) for x_i in x.T]).T
        if self.gpu and fit:  # Don't return GPU matrices when only transforming
            x = cm.CUDAMatrix(x)
        return x

    def invert(self, x):
        """Invert the preprocessing step to get x's in the original space."""
        if self.gaussianize == 'standard':
            return self.theta[1] * x + self.theta[0]
        elif self.gaussianize == 'outliers':
            return self.theta[1] * g_inv(x) + self.theta[0]
        else:
            return x
        
    def g(x, t=4):
        """A transformation that suppresses outliers for a standard normal."""
        xp = np.clip(x, -t, t)
        diff = np.tanh(x - xp)
        return xp + diff


    def g_inv(x, t=4):
        """Inverse of g transform."""
        xp = np.clip(x, -t, t)
        diff = np.arctanh(np.clip(x - xp, -1 + 1e-10, 1 - 1e-10))
        return xp + diff


    def mean_impute(x, v):
        """Missing values in the data, x, are indicated by v. Wherever this value appears in x, it is replaced by the
        mean value taken from the marginal distribution of that column."""
        if not np.isnan(v):
            x = np.where(x == v, np.nan, x)
        x_new = []
        n_obs = []
        for i, xi in enumerate(x.T):
            missing_locs = np.where(np.isnan(xi))[0]
            xi_nm = xi[np.isfinite(xi)]
            xi[missing_locs] = np.mean(xi_nm)
            x_new.append(xi)
            n_obs.append(len(xi_nm))
        return np.array(x_new).T, np.array(n_obs)


    def random_impute(x, v):
        """Missing values in the data, x, are indicated by v. Wherever this value appears in x, it is replaced by a
        random value taken from the marginal distribution of that column."""
        if not np.isnan(v):
            x = np.where(x == v, np.nan, x)
        x_new = []
        for i, xi in enumerate(x.T):
            missing_locs = np.where(np.isnan(xi))[0]
            xi_nm = xi[np.isfinite(xi)]
            xi[missing_locs] = np.random.choice(xi_nm, size=len(missing_locs))
            x_new.append(xi)
        return np.array(x_new).T
    
    

def main():
    pass


if __name__ == "__main__":
    main()

        